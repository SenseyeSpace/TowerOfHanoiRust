use std::{u8, usize};
use tower::Tower;

pub struct State {
    towers: Vec<Tower>,
}

impl State {
    pub fn new(towers: Vec<Tower>) -> State {
        return State { towers };
    }

    pub fn can_move_between(&self, from_index: usize, to_index: usize) -> bool {
        let from = &self.towers[from_index];

        if from.empty() {
            return false;
        }

        let to = &self.towers[to_index];

        return to.empty() || from.top() < to.top();
    }

    pub fn move_between(&mut self, from_index: usize, to_index: usize) -> u8 {
        let disk = self.towers[from_index].take();
        self.towers[to_index].put(disk);
        return disk;
    }

    pub fn clone(&self) -> State {
        let mut towers = vec![];

        for tower in self.towers.iter() {
            towers.push(tower.clone());
        }

        return State { towers };
    }

    pub fn hash(&self) -> String {
        let mut hash: String = "".to_owned();
        let delimiter = ";";
        for tower in self.towers.iter() {
            hash = hash + &tower.hash().to_string() + delimiter;
        }
        return hash;
    }

    pub fn len(&self) -> usize {
        return self.towers.len();
    }
}

#[cfg(test)]
mod state_test {
    use tower::Tower;
    use state::State;

    fn get_state() -> State {
        return State::new(vec![
            Tower::new(vec![3, 2, 1]),
            Tower::new(vec![]),
            Tower::new(vec![]),
        ]);
    }

    #[test]
    fn state_hash() {
        let state = get_state();

        assert_eq!(state.hash(), "3,2,1,;;;")
    }

    #[test]
    fn state_can_move_between() {
        let state = get_state();

        assert_eq!(state.can_move_between(0, 1), true);
        assert_eq!(state.can_move_between(1, 0), false);
    }

    #[test]
    fn state_move_between() {
        let mut state = get_state();

        assert_eq!(state.move_between(0, 2), 1);
        assert_eq!(state.move_between(0, 1), 2);
        assert_eq!(state.move_between(2, 1), 1);
        assert_eq!(state.move_between(0, 2), 3);

        assert_eq!(state.hash(), ";2,1,;3,;")
    }

    #[test]
    fn state_clone() {
        let source_state = get_state();
        let mut clone_state = source_state.clone();

        assert_eq!(source_state.hash(), "3,2,1,;;;");
        assert_eq!(clone_state.hash(), "3,2,1,;;;");

        clone_state.move_between(0, 1);

        assert_eq!(source_state.hash(), "3,2,1,;;;");
        assert_eq!(clone_state.hash(), "3,2,;1,;;");
    }
}
